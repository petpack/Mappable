var marker;
(function($) {
	$(function() {
		$('.geocode_button').live("click", function() {
			var $t = $(this);
			var address_fields = $t.metadata().aFields;
			var parts = address_fields.split(',');
			var address_values = [];
			for(var i=0; i < parts.length; i++) {
				address_values.push($('[name='+parts[i]+']').val());
			}
			$.getJSON(
				$t.attr('href'),
				{address: address_values.join(" " )},
				function(data) {
					//DM: ZOMG! perhaps we should not assume that the third 
					//	party service which can fail for a number of reasons
					//	returns successfully and actually handle some errors. 
					//	I know right, I'm an alarmist.
					if ('error' in data) {
						alert(data.error);
						return false;
					}
					
					$lat.val(data.lat);
					$long.val(data.lng);
					if($lat.siblings('span.readonly').length) {
						$lat.siblings('span.readonly').text(data.lat);
					}
					if($long.siblings('span.readonly').length) {
						$long.siblings('span.readonly').text(data.lng);
					}
					
					//update map
					var point = new GLatLng($lat.val(), $long.val());
					marker.setPoint(point);
					map.setCenter(point,16);

				}
			);
			return false;
		});
		var $t = $('.geocode_button');
		var $lat = $('[name='+$t.metadata().lat+']');
		var $long = $('[name='+$t.metadata().long+']');
		var center = new GLatLng($lat.val(), $long.val());
		marker = new GMarker(center, {draggable: true});
		map.setCenter(center,16);
		map.addOverlay(marker);
		GEvent.addListener(marker, "dragend", function(overlay, point) {
			var point = marker.getLatLng();
			map.setCenter(point);
			var $lat = $('[name='+$t.metadata().lat+']');
			var $long = $('[name='+$t.metadata().long+']');
			$lat.val(point.lat());
			$long.val(point.lng());
			if($lat.siblings('span.readonly').length) {
				$lat.siblings('span.readonly').text(point.lat());
			}
			if($long.siblings('span.readonly').length) {
				$long.siblings('span.readonly').text(point.lng());
			}
		
		});
	});
})(jQuery);
